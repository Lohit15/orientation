Markdown tutorial:
1)To make to words italic use underscore(_) before and after the word.
Ex: _not_   ->  not
2)To make it bold use two asterisk(**) before and after the word.
Ex: **will** -> will
3)Headers : Six types of headers use as many hashes according to the header size you want:
EX: ####Header one (4th header)
4)Links:To create links to other websites.
A)Inline link: use [] for text and wraplink in ()
Ex: [use github](www.gitlab.com).
B)Reference link: Reference to another place in the document .
Ex: [sees something fun][“a fun place”] : www.zombo.com
5)Images:
A)Inline image link:
 ![virat kohli](data:image/jpeg)
B)Reference image: 
![Black cat][Black] : https://cat.com
6)Blackquotes: Sentence or paragraph specially formatted to draw attention to reader.
 Ex: >” coronovirus is a pandemic disease”
7)Lists:
A)unordered lists/list with bullets
 Ex: *Milk
*Eggs ( * signify space)
O/p
Milk
Eggs
B) ordered /lists with numbers
1)Milk           o/p: 1)Milk 
2)Eggs               2)Eggs

8)Pragraphs: To not represent a paragraph in single line use two space after each line.
Ex: Hi..    (.. represent spaces virtually)
   My name is lohit..
   thank you.
